# DevOps

This is a sample demo built for the Gitlab Practice Engineer Assessment. 

## Quick Overview

This is a simple python application that converts an array into JSON format. The intent of this project is to showcase how an application that is built and executed through an external CI tool (in this case, Jenkins), can be rewritten into a Gitlab CI pipeline. 

## Setup

For this project, I only used docker compose to spin up a containerized version of Jenkins in conjuction with the use of the https://gitlab.com SAAS offering. The Jenkinsfile is used for a "pipeline as code" deployment via Jenkins, and its counterpart lives in the gitlab-ci yaml file.



