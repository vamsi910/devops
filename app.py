# This simple python function converts an array of triples into JSON format
# It uses the numpy function to do some array manipulation
import numpy as np

the_array = np.array([[10, 20, 30], [40, 50, 60], [70, 80, 90]])
lists = the_array.tolist()
print("LISTS:  ", lists)
if not lists:
    print("Empty array")

elif len(lists) % 3 != 0:
    print("Please ensure your data is in triple format")

else:
    print([{'x': x[0], 'y': x[1], 'z': x[2]} for i, x in enumerate(lists)])
